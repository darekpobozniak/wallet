'use strict';

/**
 * Service that make wallet calculations
 */
app.service('Wallet', function () {
  function Wallet () {
    this.total = settings.totalDefault;
    this.events = [];
  }

  Wallet.prototype.add = function (amount) {
    this.total +=  Number(amount) || 0;
    return this.total;
  };

  Wallet.prototype.remove = function (amount) {
    this.total -=  Number(amount) || 0;
    return this.total;
  };

  Wallet.prototype.getItems = function () {
    return this;
  };

  Wallet.prototype.saveItems = function (userAmount, action) {
    var event = {
      time: new Date(),
      amount: userAmount,
      action: action
    };

    this.events.push(event);

    return this;
  };

  Wallet.prototype.resetStorage = function () {
    this.total = settings.totalDefault;
    this.events = [];
    return true;
  };

  return Wallet;
});