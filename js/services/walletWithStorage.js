'use strict';

/**
 * Service that make wallet calculations
 */
app.service('WalletWithStorage', ['Wallet', function (Wallet) {
  function WalletWithStorage () {
    this.wallet = new Wallet();
  }

  WalletWithStorage.prototype.add = function (amount) {
    this.wallet.add(amount);
    return this.wallet.total;
  };

  WalletWithStorage.prototype.remove = function (amount) {
    this.wallet.remove(amount);
    return this.wallet.total;
  };

  WalletWithStorage.prototype.getItems = function () {
    var savedItems = this.getStorage();
    var tempObject = {};

    if (savedItems !== null) {
      tempObject = JSON.parse(savedItems);

      this.wallet.total = tempObject.total;
      this.wallet.events = tempObject.events;
    }

    return this.wallet;
  };

  WalletWithStorage.prototype.saveItems = function (userAmount, action) {
    this.wallet.saveItems(userAmount, action);

    // save items to localStorage
    this.setStorage(JSON.stringify(this.wallet));

    return this.wallet;
  };

  WalletWithStorage.prototype.getStorage = function () {
    return localStorage.getItem(settings.storageKey);
  };

  WalletWithStorage.prototype.setStorage = function (element) {
    localStorage.setItem(settings.storageKey, element);
    return true;
  };

  WalletWithStorage.prototype.resetStorage = function () {
    this.wallet.resetStorage();
    localStorage.removeItem(settings.storageKey);
    return true;
  };

  return WalletWithStorage;
}]);