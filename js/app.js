'use strict';

/**
 * The main Wallet app module
 * @type {angular.Module}
 */
var app = angular.module('wallet', []);

var settings = {
  storageKey: 'wallet',
  menuToggleClass: 'main-nav--open',
  totalDefault: 100,
  menuElement: '.main-nav'
};