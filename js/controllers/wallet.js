'use strict';

/**
 * The main controller for the app
 * - exposes the model to the template and provides event handlers
 */
app.controller('WalletController', ['$scope', 'WalletWithStorage', function ($scope, Wallet) {
  var Wallet = new Wallet();
  var menu = document.querySelector(settings.menuElement);
  var items = Wallet.getItems(settings.storageKey);
  var displayData = function (total, events) {
    $scope.total = total;
    $scope.events = events;
  };

  $scope.action = 'add';

  displayData(items.total, items.events);

  var createEvent = function () {
    var data = Wallet.saveItems($scope.userAmount, $scope.action);

    displayData(data.total, data.events);
    $scope.userAmount = '';
  };

  $scope.sendForm = function () {
    var action = $scope.action;

    if ($scope.userAmount) {
      $scope[action]();
      $scope.errorEmptyField = false;
    } else {
      $scope.errorEmptyField = true;
    }
  };

  $scope.add = function () {
    var total = Wallet.add($scope.userAmount);
    createEvent();
  };

  $scope.remove = function () {
    var total = Wallet.remove($scope.userAmount);
    if (total < 0) {
      $scope.errorTooMuchMoney = true;
    } else {
      $scope.errorTooMuchMoney = false;
      createEvent();
    }
  };

  $scope.reset = function () {
    Wallet.resetStorage();
    displayData(settings.totalDefault, []);
  };

  $scope.toggleMenu = function () {
    menu.classList.toggle(settings.menuToggleClass);
  };
}]);