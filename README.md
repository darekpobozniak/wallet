# Wallet App #

A simple wallet application which allows you to add and remove amounts to and from your wallet.

## How do I get set up? ##

### 1. Install sass ###
``` bash
  $ [sudo] gem install sass
```

### 2. Install gulp globally ###
``` bash
  $ npm install --global gulp
```

### 3. Clone the git repository ###
``` bash
  $ git clone git@bitbucket.org:darekpobozniak/wallet.git
```

### 4. Install gulp in your project devDependencies: ###
``` bash
  $ cd wallet
  $ npm install
```

### 5. Start gulp to watch your sass changes ###
``` bash
  $ gulp
```