var gulp = require('gulp');
var sass = require('gulp-ruby-sass');

gulp.task('styles', function () {
  return sass('styles/main.scss', { style: 'expanded' })
    .on('error', function (err) {
      console.error('Error!', err.message);
    })
    .pipe(gulp.dest('dist/assets/css'));
});

gulp.task('default', function () {
  gulp.start('styles');
});

gulp.watch('styles/main.scss', ['styles']);